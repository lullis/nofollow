#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='nofollow',
    url='https://bitbucket.org/lullis/nofollow',
    version='0.0.1',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'celery',
        'django>=2.0',
        'django-boris',
        'django-cindy',
        'django_compressor',
        'django-kip',
        'django-model-utils',
        'djangorestframework',
        'django-material',
        'libsass',
        'psycopg2-binary'
    ],
    zip_safe=False,
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content'
    ],
    keywords='bookmark-manager syndication html-extraction'
)
